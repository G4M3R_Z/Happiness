using UnityEngine;

public class ParticleFade : MonoBehaviour
{
    [SerializeField] private Transform trs;
    [SerializeField] private CanvasGroup icon;

    private void Start() =>
        LeanTween.value(gameObject, 1, 0, 1).
            setOnUpdate((float v) =>
            {
                trs.localPosition += Vector3.up;
                icon.alpha = v;
            }).
            setDestroyOnComplete(true);
}