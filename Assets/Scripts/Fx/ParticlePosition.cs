using UnityEngine;

public class ParticlePosition : MonoBehaviour
{
    [SerializeField] private RectTransform canvas;
    [SerializeField] private GameObject prefab;
    [SerializeField] private Transform parent;

    private void Awake() => this.enabled = false;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject instance = Instantiate(prefab, parent);
            Transform trs = instance.transform;

            Vector2 localPos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas, (Vector2)Input.mousePosition, null, out localPos);
            trs.localPosition = localPos;
        }
    }
}