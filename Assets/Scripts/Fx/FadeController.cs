using UnityEngine;
using UnityEngine.SceneManagement;

public enum SceneName
{
    None = -1,
    Menu = 0,
    Gameplay = 1,
    MiniGame = 2,
    Credits = 3,
    Exit = 4
}
public class FadeController : MonoBehaviour
{
    [SerializeField] [Range(0, 1)] private float time;
    [SerializeField] private CanvasGroup canvasGroup;
    private SceneName sceneName = SceneName.None;

    private void Start()
    {
        int sceneType = (int)sceneName;

        int start = (sceneType >= 0) ? 0 : 1;
        int end = (sceneType >= 0) ? 1 : 0;

        canvasGroup.alpha = start;

        LTDescr tween = LeanTween.alphaCanvas(canvasGroup, end, time);
        tween.setOnComplete(() =>
        {
            if (sceneType == (int)SceneName.Exit) Application.Quit();
            else if (sceneType < 0) Destroy(gameObject);
            else SceneManager.LoadScene(sceneType);
        });
    }
    public void SetSceneType(SceneName scene) => sceneName = scene;
}