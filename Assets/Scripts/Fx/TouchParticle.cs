using UnityEngine;

public class TouchParticle : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private GameObject prefab;
    [SerializeField] private Transform parent;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 pos = cam.ScreenToWorldPoint(Input.mousePosition);
            Instantiate(prefab, pos, Quaternion.identity, parent);
        }
    }
}