using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    public static int gameId;
    public static bool infinity;
    public static bool complete;

    [SerializeField] private RectTransform canvas;
    [SerializeField] private GameObject[] games;

    private void Awake() { if (gameId != 0) games[gameId - 1].SetActive(true); }
    private void Start() => complete = false;

    //Canvas
    public RectTransform CanvasRect() { return canvas; }

    //Event System
    public void SetFunctions(Transform item, UnityEvent actions)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) => { actions.Invoke(); });
        item.GetComponent<EventTrigger>().triggers.Add(entry);
    }
}