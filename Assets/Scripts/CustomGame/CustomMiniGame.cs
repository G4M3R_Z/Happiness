using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SphereCollider))]
public class CustomMiniGame : MonoBehaviour
{
    [SerializeField] private UnityEvent enter, exit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) enter.Invoke();
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) exit.Invoke();
    }
}