using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SphereCollider))]
public class NpcController : MonoBehaviour
{
    [SerializeField] private int id;
    [SerializeField] private MeshRenderer mesh;
    [SerializeField] private UnityEvent enter, exit;

    public void ChangeMaterial(Material mat) => mesh.material = mat; 
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.gameId = id;
            enter.Invoke();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.gameId = 0;
            exit.Invoke();
        }
    }
}