using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WinCondition : MonoBehaviour
{
    [SerializeField] [Range(0, 10)] private int islandCount;
    [SerializeField] private UnityEvent gameComplete;

    private List<GameObject> islands = new List<GameObject>();
    private GameObject current;

    public void AddCurrentIsland(GameObject island)
    {
        current = island;
    }
    public void CompareCompleteIslands()
    {
        if (islands.Contains(current)) return;
        islands.Add(current);

        if (islands.Count == islandCount) gameComplete.Invoke();
    }
}