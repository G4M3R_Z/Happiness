using UnityEngine;

public class Movement : MonoBehaviour
{
    public static Vector3 initialPosition, initialRotation;

    [SerializeField] [Range(0, 10)] private float speed;
    [SerializeField] private FixedJoystick joystick;
    [SerializeField] private Transform render;
    [SerializeField] private CharacterController character;
    
    [SerializeField] private Animator anim;
    private string state;

    private void Awake()
    {
        if (initialPosition != Vector3.zero) SetCharacterPosition();
        if (initialRotation != Vector3.zero) SetRenderRotation();
    }
    private void FixedUpdate()
    {
        Vector2 movement = joystick.Direction;

        AnimationState(movement != Vector2.zero ? "Up" : "Down");

        Vector3 dir = new Vector3(movement.x, 0, movement.y);
        character.Move(dir * Time.fixedDeltaTime * speed);

        float angle = Vector3.Angle(Vector3.forward, dir);
        angle = dir.x < 0 ? -angle : angle;

        if(movement != Vector2.zero)
        {
            Vector3 rot = render.localEulerAngles;
            rot.y = Mathf.LerpAngle(rot.y, angle, Time.deltaTime * 5);
            render.localEulerAngles = rot;
        }
    }
    private void AnimationState(string animationName)
    {
        if (state == animationName) return;
        anim?.Play(animationName);
        state = animationName;
    }
    private void SetCharacterPosition()
    {
        character.enabled = false;
        transform.position = initialPosition;
        character.enabled = true;
    }
    private void SetRenderRotation()
    {
        render.eulerAngles = initialRotation;
    }
    public void SaveLocation()
    {
        initialPosition = transform.position;
        initialRotation = render.eulerAngles;
    }
}