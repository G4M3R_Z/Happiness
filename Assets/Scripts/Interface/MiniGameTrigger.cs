using UnityEngine;
using UnityEngine.Events;

public class MiniGameTrigger : MonoBehaviour
{
    [SerializeField] private UnityEvent enter, exit;

    private void OnTriggerEnter(Collider other)
    {
        enter.Invoke();
    }
    private void OnTriggerExit(Collider other)
    {
        exit.Invoke();
    }
}