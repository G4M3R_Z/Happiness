using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    [SerializeField] private GameObject fade;
    [SerializeField] private SceneName[] scene;

    public void SceneButton(int index) => Instantiate(fade, transform).GetComponent<FadeController>().SetSceneType(scene[index]);
    public void MiniGameScene() => SceneManager.LoadScene((int)SceneName.MiniGame);
    public void BackToGameplay() => SceneManager.LoadScene((int)SceneName.Gameplay);
}