using UnityEngine;
using UnityEngine.UI;

public class InteractButton : MonoBehaviour
{
    [SerializeField] private CanvasGroup alpha;
    [SerializeField] private Button interactable;
    [SerializeField] [Range(0, 1)] private float min, max, time;

    private void Awake() { alpha.alpha = min; interactable.interactable = false; }
    public void Show() { LeanTween.alphaCanvas(alpha, max, time); interactable.interactable = true; }
    public void Hide() { LeanTween.alphaCanvas(alpha, min, time); interactable.interactable = false; }
}