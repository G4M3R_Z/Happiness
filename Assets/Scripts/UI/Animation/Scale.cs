using UnityEngine;

public class Scale : MonoBehaviour
{
    [SerializeField] [Range(0, 1)] private float time;
    [SerializeField] [Range(0, 1)] private float startSize;
    [SerializeField] private LeanTweenType curve;

    void Start()
    {
        transform.localScale = Vector3.one * startSize;
        LeanTween.scale(gameObject, Vector3.one, time).setEase(curve).setLoopPingPong();
    }
}