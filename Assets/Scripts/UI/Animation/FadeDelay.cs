using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class FadeDelay : MonoBehaviour
{
    [SerializeField] private GameObject fade;
    [SerializeField] private UnityEvent start, complete;

    private void Awake() => StartCoroutine(Wait());
    private IEnumerator Wait()
    {
        start.Invoke();
        yield return new WaitUntil(() => fade == null);
        complete.Invoke();
    }
}