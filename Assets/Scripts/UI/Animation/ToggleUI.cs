using UnityEngine;
using UnityEngine.UI;

public class ToggleUI : MonoBehaviour
{
    [SerializeField] private Toggle toggle;
    [SerializeField] private RectTransform select;
    [SerializeField] private Image icon;
    [SerializeField] private Sprite enable, disable;


    [Header("Animation")]
    [SerializeField] [Range(0, 1)] private float time;
    [SerializeField] private LeanTweenType curve;

    private void Awake() => toggle.isOn = GameManager.infinity;
    public void Infinity() => GameManager.infinity = toggle.isOn;
    public void Icon() => icon.sprite = toggle.isOn ? enable : disable;
    public void Swipe() => LeanTween.moveLocalX(select.gameObject, -select.localPosition.x, time).setEase(curve);
}