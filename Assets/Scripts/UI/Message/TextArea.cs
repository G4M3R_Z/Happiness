using UnityEngine;
using TMPro;

public class TextArea : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] [TextArea] private string info;

    private void Start() => text?.SetText(info);
}