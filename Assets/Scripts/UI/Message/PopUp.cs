using UnityEngine;

public class PopUp : MonoBehaviour
{
    [SerializeField] [Range(0, 1)] private float time;
    [SerializeField] [Range(0, 10)] private float delay;
    [SerializeField] private LeanTweenType curve;

    private void Start()
    {
        transform.localScale = Vector2.zero;
        LeanTween.scale(gameObject, Vector2.one, time).setEase(curve).setDelay(delay);
    }
    public void Close()
    {
        LeanTween.scale(gameObject, Vector2.zero, time).setEase(curve).setDestroyOnComplete(true);
    }
}