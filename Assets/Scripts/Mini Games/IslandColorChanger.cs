using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.PostProcessing;

public class IslandColorChanger : MonoBehaviour
{
    [SerializeField] private int id;
    [SerializeField] private ProgressionComplete progress;

    [Space]
    [SerializeField] private Transform player;
    [SerializeField] [Tooltip("Distance in Meters")] [Range(0, 50)] private float distance;
    private bool complete, inside;
    
    [Space]
    [SerializeField] private PostProcessVolume profile;
    private ColorGrading color;
    
    [Space]
    [SerializeField] private Color on, off;
    [SerializeField] [Range(0, 5)] private float delay, time;
    [SerializeField] private GameObject[] elements;
    [SerializeField] private UnityEvent finish;

    private void Awake()
    {
        complete = progress.ItemInside(id);
        profile.profile.TryGetSettings(out color);

        float playerDistance = Vector3.Distance(transform.position, player.position);
        inside = playerDistance < distance;

        ChangeItemsColors(off, 0, 0);
        if (inside) color.saturation.value = -70;

        if (!complete)
        {
            if(id == GameManager.gameId && GameManager.complete)
            {
                progress.AddCompleteIsland(id);
                ChangeItemsColors(on, delay, time);
                ChangeSaturation(-70, 0, 1, delay);
                finish.Invoke();
                complete = true;
            }
        }
        else
        {
            if(inside) color.saturation.value = 0;
            ChangeItemsColors(on, 0, 0);
            finish.Invoke();
        }
    }
    private void FixedUpdate()
    {
        if (complete) return;
        float playerDistance = Vector3.Distance(transform.position, player.position);

        if (!inside)
        {
            if(playerDistance < distance)
            {
                ChangeSaturation(0, -70, 1, 0);
                inside = true;
            }
        }
        else
        {
            if(playerDistance > distance)
            {
                ChangeSaturation(-70, 0, 1, 0);
                inside = false;
            }
        }
    }
    private void ChangeSaturation(float from, float to, float time, float delay)
    {
        LeanTween.value(from, to, time).setOnUpdate((float v) => color.saturation.value = (int)v).setDelay(delay);
    }
    private void ChangeItemsColors(Color color, float delay ,float time)
    {
        LTSeq seq = LeanTween.sequence();
        seq.append(delay);
        seq.append(() => { foreach (var item in elements) LeanTween.color(item, color, time); });
        seq.append(() => GameManager.complete = false);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, distance);
    }
}