using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProgressionComplete : MonoBehaviour
{
    [SerializeField] [Range(0, 10)] private int count;
    
    [SerializeField] GameObject fadeInScene;
    [Space][SerializeField] private UnityEvent start;
    [SerializeField] private UnityEvent firstTime, completeGame;

    static public List<int> progress = new List<int>();

    private void Start()
    {
        start.Invoke();
        if (progress.Count == 0) StartCoroutine(DelayAction(firstTime));
        else if (progress.Count == count) StartCoroutine(DelayAction(completeGame));
    }
    private IEnumerator DelayAction(UnityEvent action)
    {
        yield return new WaitUntil(() => fadeInScene == null);
        action.Invoke();
    }
    public void AddCompleteIsland(int id) => progress.Add(id);
    public bool ItemInside(int id) { return progress.Contains(id); }
}