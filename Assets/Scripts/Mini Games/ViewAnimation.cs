using UnityEngine;

public class ViewAnimation : MonoBehaviour
{
    [SerializeField] private SceneController scenes;
    [SerializeField] [Range(0, 1)] private float time;

    private bool changeScene, enable;
    private static Vector2 startPos;
    private Vector2 endPos;

    private void Awake() => enable = GameManager.gameId != 0;
    private void Start()
    {
        if (!enable) startPos = transform.localPosition;
        else Hide();
    }

    public void Show()
    {
        if (changeScene) return;

        changeScene = true;
        transform.localPosition = startPos;
        transform.localScale = Vector2.one / 1.3f;

        LTSeq seq = LeanTween.sequence();
        seq.append(3);
        seq.append(LeanTween.moveLocal(gameObject, endPos, time).setEase(LeanTweenType.easeOutBack));
        seq.append(LeanTween.scale(gameObject, Vector2.one, time).setEase(LeanTweenType.easeOutQuint));
        seq.append(() => scenes.MiniGameScene());
    }
    public void Hide()
    {
        transform.localPosition = endPos;
        transform.localScale = Vector2.one;

        LTSeq seq = LeanTween.sequence();
        seq.append(LeanTween.scale(gameObject, Vector2.one / 1.3f, time).setEase(LeanTweenType.easeInQuint));
        seq.append(LeanTween.moveLocal(gameObject, startPos, time).setEase(LeanTweenType.easeInBack));
    }
}