using UnityEngine;
using UnityEngine.Events;

public class MovementBar : MonoBehaviour
{
    [SerializeField] [Range(0, 20)] private float speed;
    [SerializeField] private RectTransform bar;
    [SerializeField] private RectTransform point;
    [SerializeField] private UnityEvent pressed;

    private float limit;
    private bool left;

    private void Start() => limit = (bar.rect.width / 2) - (point.rect.width / 2);
    private void FixedUpdate()
    {
        Vector2 move = point.position;
        Vector2 dir = left ? Vector2.left : Vector2.right;
        move += dir * speed * 2 * Time.fixedDeltaTime;
        point.position = move;

        Vector2 pos = point.localPosition;
        pos.x = Mathf.Clamp(pos.x, -limit, limit);
        if(pos.x == -limit || pos.x == limit) left = !left;
        point.localPosition = pos;
    }
    public void Center()
    {
        float percentage = 0.3f;
        float amount = point.localPosition.x;
        if (amount > -limit * percentage && amount < limit * percentage)
            pressed.Invoke();
    }
}