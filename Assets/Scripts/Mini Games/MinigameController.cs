using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MinigameController : MonoBehaviour
{
    public static MinigameController ctrl;

    [Header("Animation")]
    [SerializeField] private Transform trs;
    [SerializeField] [Range(0, 1)] private float time;
    [SerializeField] [Range(0, 5)] private float delay;
    private Vector2 size = Vector2.one * 0.7f;
    private Vector2 pos;

    private GameObject minigame;
    [SerializeField] private List<GameObject> doors = new List<GameObject>();

    [Header("Conditions")]
    [SerializeField] private UnityEvent open;
    [SerializeField] private UnityEvent close;
    [SerializeField] private UnityEvent clear;
    [SerializeField] private UnityEvent complete;

    private void Awake() { ctrl = this; trs = transform; }
    private void Start() => pos = trs.localPosition;

    public void SetGame(GameObject game) => minigame = game;
    public void SetDoorToOpen(GameObject newDoor) => doors.Add(newDoor);
    public void RemoveDoorsToOpen(GameObject currentDoor) => doors.Remove(currentDoor);

    public void DestroyGame() { if (transform.childCount > 1) Destroy(transform.GetChild(1).gameObject); }
    public void DestroyDoor() { if (doors.Count > 0) foreach (var item in doors) { Destroy(item); } }

    public void Show()
    {
        if (trs.childCount > 1) Destroy(trs.GetChild(1).gameObject);
        if (minigame != null) Instantiate(minigame, trs);

        trs.localScale = size;
        LTSeq seq = LeanTween.sequence();

        seq.append(delay);
        seq.append(LeanTween.moveLocalY(gameObject, 0, time).setEase(LeanTweenType.easeOutBack));
        seq.append(LeanTween.scale(gameObject, Vector2.one, time).setEase(LeanTweenType.easeOutQuint));
        seq.append(() => open.Invoke());
    }
    public void Hide()
    {
        LTSeq seq = LeanTween.sequence();

        seq.append(LeanTween.scale(gameObject, size, time).setEase(LeanTweenType.easeInQuint));
        seq.append(LeanTween.moveLocalY(gameObject, pos.y, time).setEase(LeanTweenType.easeInBack));
        seq.append(() => clear.Invoke());
        seq.append(() => close.Invoke());
        seq.append(delay);

        StressBar bar = trs.GetComponentInChildren<StressBar>();
        //if (GameManager.ctrl.IsInfinite()) return;
        complete.Invoke();
    }
}