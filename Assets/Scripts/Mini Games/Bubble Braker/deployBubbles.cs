using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class deployBubbles : MonoBehaviour
{
    public UnityEvent evento;
    public GameObject bubblePrefab;
    public float maxRespawnTime = 1.3f;
    public float minRespawnTime = 0.5f;
    private Vector3 screenBounds;
    [SerializeField] private RectTransform canvas;
    [SerializeField] private StressBar progressBar;
    [SerializeField] private AudioSource bubbleSound;

    // Start is called before the first frame update
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        StartCoroutine(bubbleWave());
    }

    private void spawnBubble()
    {
        float width = canvas.sizeDelta.x / 2;
        float heigth = canvas.sizeDelta.y / 2;
        GameObject a = Instantiate(bubblePrefab, new Vector3(Random.Range(bubblePrefab.GetComponent<RectTransform>().rect.width, (width * 3) - bubblePrefab.GetComponent<RectTransform>().rect.width), heigth * (3f), 0), Quaternion.identity, GameObject.FindGameObjectWithTag("SpawnArea").transform) as GameObject;
        a.GetComponent<Button>().onClick.AddListener(() => bubbleSound.Play());
        a.GetComponent<Button>().onClick.AddListener(() => progressBar.ReduceAmount(5));
        a.GetComponent<Button>().onClick.AddListener(() => Destroy(a));
    }

    IEnumerator bubbleWave()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minRespawnTime, maxRespawnTime));
            spawnBubble();
        }
    }

    void Update()
    {

    }

}
