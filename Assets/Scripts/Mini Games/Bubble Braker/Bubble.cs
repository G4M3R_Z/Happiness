using UnityEngine;

public class Bubble : MonoBehaviour
{
    [SerializeField] private Transform trs;
    [SerializeField] [Range(0, 15)] private float speed;

    private void FixedUpdate() => trs.position += Vector3.down * speed * Time.fixedDeltaTime;
    private void Update() { if (trs.position.y < -7) Destroy(this.gameObject); }
}