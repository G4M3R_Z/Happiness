using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class BubbleSpawner : MonoBehaviour
{
    [SerializeField] private GameManager ctrl;

    [SerializeField] private GameObject prefab;
    [SerializeField] [Range(0, 5)] private float minTime, maxTime;
    [SerializeField] private UnityEvent touch;

    private float top, area;

    private void Start()
    {
        top = ctrl.CanvasRect().rect.height / 2;
        area = ctrl.CanvasRect().rect.width / 2;

        StartCoroutine(BubbleSpawn());
    }
    private IEnumerator BubbleSpawn()
    {
        while (true)
        {
            float random = Random.Range(minTime, maxTime);
            yield return new WaitForSeconds(random);

            RectTransform bubble = Instantiate(prefab, transform).GetComponent<RectTransform>();
            Vector2 size = bubble.rect.size / 2;

            float pos = Random.Range(-area + size.x, area - size.x);
            bubble.localPosition = new Vector2(pos, top + size.y);

            UnityEvent action = new UnityEvent();
            action.AddListener(() => touch.Invoke());
            action.AddListener(() => Destroy(bubble.gameObject));
            ctrl.SetFunctions(bubble, action);
        }
    }
}