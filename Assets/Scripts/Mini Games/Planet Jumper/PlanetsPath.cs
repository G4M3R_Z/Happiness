using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PlanetsPath : MonoBehaviour
{
    [SerializeField] private GameManager ctrl;
    [SerializeField] private RectTransform player;
    [SerializeField] private RectTransform parent;
    [SerializeField] private GameObject planetPrefab;

    [Space]
    [SerializeField] [Range(0, 1)] private float time;
    [SerializeField] private LeanTweenType curve;
    [SerializeField] private UnityEvent touch;

    private List<Transform> planets = new List<Transform>();
    private float area, distance = 500;
    private int clicks = 0;

    private void Start()
    {
        area = ctrl.CanvasRect().rect.width / 2;
        planets.Add(parent.GetChild(0));

        for (int i = 0; i < 3; i++) InstantiatePath();

        StartCoroutine(Progression());
    }
    private void InstantiatePath()
    {
        RectTransform trs = Instantiate(planetPrefab, parent).GetComponent<RectTransform>();

        UnityEvent action = new UnityEvent();
        action.AddListener(() => TouchCurrent(trs));
        ctrl.SetFunctions(trs, action);
        
        float limit = area - trs.rect.width / 2;
        
        Vector2 pos;
        pos.x = Random.Range(-limit, limit);
        pos.y = planets[planets.Count - 1].localPosition.y + distance;
        trs.localPosition = pos;

        planets.Add(trs);
    }
    private IEnumerator Progression()
    {
        while (true)
        {
            Vector2 dir = (planets[1].localPosition - planets[0].localPosition).normalized;
            float angle = Vector2.Angle(Vector2.up, dir);
            LeanTween.rotateZ(planets[0].gameObject, dir.x < 0 ? angle : -angle, time).setEase(curve);

            //Limit Clickcs Per Planeta
            yield return new WaitUntil(() => clicks >= 1);
            player?.SetParent(planets[1].GetChild(0));

            Vector3 current = parent.localPosition;
            LeanTween.moveLocal(player.gameObject, Vector2.zero, 0.2f).setOnUpdate((float v) =>
            {
                parent.localPosition = current + (Vector3.down * (distance * v));
            }).
            setOnComplete(() =>
            {
                GameObject planet = planets[0].gameObject;
                planets.Remove(planets[0]);
                
                Destroy(planet);

                InstantiatePath();
                clicks = 0;
            });

            yield return new WaitUntil(() => clicks == 0);
        }
    }
    public void TouchCurrent(Transform planet)
    {
        if (planet != planets[0]) return;
        touch.Invoke();
        clicks++;
    }
}