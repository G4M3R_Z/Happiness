using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class StressBar : MonoBehaviour
{
    [Header("Shake")]
    [SerializeField] private Transform progressBar;
    [SerializeField] [Range(0, 10)] private float force;
    private Vector2 currentPosition;

    [Header("Bar Amount")]
    [SerializeField] private Image bar;
    [SerializeField] private Color good, bad;
    [SerializeField] [Range(0, 10)] private float speed;

    [SerializeField] private UnityEvent complete;
    private bool infinity;

    private void Awake() => infinity = GameManager.infinity;
    private void Start() => currentPosition = progressBar.localPosition;
    private void Update()
    {
        float current = bar.fillAmount;
        float velocity = Time.deltaTime * speed;
        bar.fillAmount += velocity / 100f;

        //Color Changing
        bool badArea = current > 0.5f;
        bar.color = Color.Lerp(bar.color, badArea ? bad : good, Time.deltaTime);

        //Shake
        Vector2 shake = Random.insideUnitSphere * current * force;
        progressBar.localPosition = currentPosition + shake;
    }
    public void ReduceAmount(float amount)
    {
        bar.fillAmount -= amount / 100f;
        if (bar.fillAmount == 0 && !infinity)
        {
            GameManager.complete = true;
            complete.Invoke();
        }
    }
}