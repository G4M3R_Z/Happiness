using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private Transform player;
    [SerializeField] private Vector3 distance;

    private Transform cam;

    private void Awake() => cam = transform;
    private void Update() => cam.position = player.position + distance;

    public void UnFocus()
    {
        anim.Play("FocusOff");
    }
}